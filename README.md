# Integration Sample Project
This repository contains an integration sample project to get you started with building a Lotic integration.

## Prerequisites
* [Docker](https://www.docker.com)
* Python 3.6
* virtualenv
* git

## Getting Started

#### 1. Clone the repository
```
git clone https://bitbucket.org/lotic/sample-integration integration
cd integration
```

#### 2. Setup the virtualenv
```
virtualenv -p python3 .venv
source .venv/bin/activate
pip3 install -r requirements.txt
```

#### 3. Download the .env file for your integration
You can find the `.env` file in the `Resources` tab of the integration detail page in DataDocs.

#### 4. Test connectivity and run
```
docker-compose build
docker-compose up
```
After running the application starts, test that you can connect to the health check url in the browser. Use Docker to determine the assigned port for the service.

#### 5. Send test event
Integrations respond to asynchronous events sent via AWS SQS queues. To send the `test-event.json` file as an event, use the command below while your app is running.
```
./run publish
```

#### 6. Add database tables and revisions
Update the models in `app/model.py`, then create a revision:
```
./run.sh alembic revision --autogenerate -m "revision message"
./run.sh alembic
```

#### 7. Commit your changes and push
Update the models in `app/model.py`, then create a revision:
```
git commit -a "integration update"
git push
```

#### 8. Monitor the build in DataDocs
Go to the integrations detail page to find the URLs for the AWS console pages where you can monitor the application deployment.