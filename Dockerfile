FROM python:3.5.2
RUN mkdir -p /app
WORKDIR /usr/src/app
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt
COPY . /usr/src/app
EXPOSE 80
ENTRYPOINT ["python", "-m", "app"]
CMD ["upgrade"]
