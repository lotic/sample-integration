#!/usr/bin/env bash

# Environment variables for integration
source ./.env

# Python path
export PYTHONPATH=$(pwd)

# Handle commands
case $1 in
  app)
    shift
    python -m app $@
    ;;
  publish)
    aws sns publish --topic-arn ${SNS_TOPIC_ARN} --message "$(cat test-event.json)"
    ;;
  alembic)
    shift
    alembic $@
    ;;
  *)
    echo 'invalid command'
    ;;
esac

