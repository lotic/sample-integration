from __future__ import with_statement
from os import environ
from alembic import context
from logging.config import fileConfig
import sys

from app.db import engine
from app.model import metadata

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

__DB_SCHEMA__ = environ.get('DB_SCHEMA')


def include_schemas(names):
    # produce an include object function that filters on the given schemas
    def include_object(object, name, type_, reflected, compare_to):
        print(object, name, type_)
        if type_ == "table":
            return object.schema in names
        return True
    return include_object


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    with engine.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=metadata,
            include_schemas=True,
            include_object=include_schemas([__DB_SCHEMA__]),
            version_table_schema=__DB_SCHEMA__,
        )
        with context.begin_transaction():
            connection.execute('set search_path to "{}"'.format(__DB_SCHEMA__))
            context.run_migrations()

if context.is_offline_mode():
    print("Can't run migrations offline")
    sys.exit(1)
else:
    run_migrations_online()
