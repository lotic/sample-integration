import argparse
import logging
from subprocess import call
from os import environ, path
from concurrent.futures import ThreadPoolExecutor

from app.api import api
from app.worker import Worker

# Logging
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    # Parse command line arguments
    commands = ['upgrade']
    parser = argparse.ArgumentParser(description='Integration App')
    parser.add_argument('commands', nargs='*', metavar='COMMANDS', help='commands to run', choices=commands)
    args = parser.parse_args()
    try:
        if 'upgrade' in args.commands:
            logger.info('starting database upgrade')
            environ['PYTHONPATH'] = path.dirname(path.dirname(__file__))
            call(['alembic', 'upgrade', 'head'])
            logger.info('finished database upgrade')
        # Run the app
        logger.info('running app')
        with ThreadPoolExecutor(max_workers=2) as executor:
            executor.submit(Worker(environ.get('SQS_QUEUE_URL')))
            executor.submit(api.run, host='0.0.0.0', port=5050)
    except KeyboardInterrupt:
        pass
