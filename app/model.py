from sqlalchemy import MetaData, Table, Column, Integer, String, Enum

from .dbenv import __DB_SCHEMA__

metadata = MetaData(schema=__DB_SCHEMA__)

# Sample table for storing people with an ID and name
people = Table('people', metadata,
    Column('employee_id', Integer, primary_key=True, autoincrement=False),
    Column('first_name',  String),
    Column('last_name',   String),
)
