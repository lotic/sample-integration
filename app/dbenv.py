import logging
from os import environ
from dotenv import load_dotenv, find_dotenv


# Load environment variables
try:
    dot_env_file = find_dotenv(raise_error_if_not_found=True)
    load_dotenv(dot_env_file)
except IOError as e:
    logging.warning('.env file not found, using environment variables: %s', str(e))

# Mapping of environment variables to psycopg2 parameters
__ENV_VAR_MAP__ = [
    ('DB_HOST', 'host'),
    ('DB_NAME', 'dbname'),
    ('DB_USER', 'user'),
    ('DB_PASS', 'password'),
]
# Database schema
__DB_SCHEMA__ = environ.get('DB_SCHEMA')
# Load environment variables
db_params = {param: environ.get(env_var) for (env_var, param) in __ENV_VAR_MAP__}