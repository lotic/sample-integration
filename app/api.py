from flask import Flask, jsonify

# TODO: Use the DB layer
from sqlalchemy import select
from .db import engine
from .model import people


# Create the Flask application
api = Flask(__name__)
api.url_map.strict_slashes = False


@api.route('/')
def health_check():
    """Required health check URL"""
    return jsonify({'status': 'ok'})


# TODO: Implement your routes here

# Example
@api.route('/people')
def people():
    with engine.connect() as conn:
        result = conn.execute(select(people))
    return jsonify(list(result))
