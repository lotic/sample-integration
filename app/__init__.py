import logging
import sys

logging.basicConfig(
    level=logging.INFO,
    format="%(levelname)s\t[%(threadName)s %(name)s.%(funcName)s:%(lineno)d] %(message)s",
    stream=sys.stdout,
)
