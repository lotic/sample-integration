import boto3
import json
import logging
import requests
from retrying import retry

logger = logging.getLogger(__name__)


class Worker:
    """Class that polls a queue and responds to messages."""

    def __init__(self, queue_url):
        """Create an AWS SQS message poller."""
        super().__init__()
        self.queue = boto3.resource('sqs').Queue(queue_url)
        self.opts = dict(WaitTimeSeconds=20, MessageAttributeNames=['All'])

    def __call__(self, *args, **kwargs):
        """Default behavior of this callable is to poll for messages."""
        self.poll_messages()

    def poll_messages(self):
        """Receives custom resource requests from message queue and performs provisioning."""
        logger.info('polling for messages on queue %s', self.queue.url)
        while True:
            for message in self.queue.receive_messages(**self.opts):
                try:
                    self.__process_message(message)
                except Exception as e:
                    logger.error('Unable to process message: {}'.format(str(e)))
                finally:
                    message.delete()

    def __process_message(self, message):
        """Process an individual message."""
        message_body = json.loads(message.body)
        self.__process_message_body(message_body)

    def __process_message_body(self, message_body):
        """Process the request, the Message within the message_body."""
        request = message_body['Message']
        request = self.read_request(request)
        self.process_request(request)

    def read_request(self, request):
        """Preprocess the request, read JSON or a file format."""
        return json.loads(request)

    @retry(stop_max_attempt_number=3)
    def respond_to_request(self, response_url, response):
        requests.put(response_url, json=response).raise_for_status()

    def __do_process_request(self, request):
        """Abstract method to process request."""
        event_type = request.get('event_type')
        logger.info('received event of type: %s', event_type)
        data_source = request.get('data_source')
        if data_source:
            logger.info('data source sync request: (%s) %s', data_source['id'], data_source['name'])
            attachments = data_source['attachments']
            if attachments:
                logger.info('data source has eligible attachments to integrate')
                for attachment in attachments:
                    logger.info('integrating attachment: %s', attachment['value'])
                    self.integrate_attachment(data_source, attachment)
        else:
            logger.warning('sync request does not contain a data source to target')
            return {'status': 'F', 'message': 'missing data source'}
        # Finally, respond to the event
        return {'status': 'S'}

    def process_request(self, request):
        """Abstract method to process request."""
        response_url = request.get('response_url')
        try:
            result = self.__do_process_request(request)
        except Exception as e:
            result = {'status': 'failed', 'message': str(e)}
        if response_url:
            try:
                self.respond_to_request(response_url, result)
            except Exception as e:
                logger.error('unable to send message response: %s', str(e))
        else:
            logger.warning('message was missing response_url for result: %s', json.dumps(result))

    def integrate_attachment(self, data_source, attachment):
        """Integrate an attachment."""
        # TODO: Add integration logic here
        pass