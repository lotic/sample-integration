import sys
import logging
from psycopg2.pool import ThreadedConnectionPool
from sqlalchemy import create_engine

from .dbenv import db_params

logger = logging.getLogger(__name__)


try:
    logger.info('creating PostgreSQL connection pool')
    __CONN_POOL__ = ThreadedConnectionPool(1, 10, **db_params)
    engine = create_engine('postgresql+psycopg2://', creator=__CONN_POOL__.getconn)
except KeyboardInterrupt:
    pass
except Exception as e:
    logger.error('unable to create connection pool: %s', str(e))
    sys.exit(3)
